package com.cobra.yugal2;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.yalantis.ucrop.UCrop;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {


    static {
        System.setProperty(
                "org.apache.poi.javax.xml.stream.XMLInputFactory",
                "com.fasterxml.aalto.stax.InputFactoryImpl"
        );
        System.setProperty(
                "org.apache.poi.javax.xml.stream.XMLOutputFactory",
                "com.fasterxml.aalto.stax.OutputFactoryImpl"
        );
        System.setProperty(
                "org.apache.poi.javax.xml.stream.XMLEventFactory",
                "com.fasterxml.aalto.stax.EventFactoryImpl"
        );
    }

    private Button aadharButton, panButton;

    private CheckBox aadharCheck, panCheck;

    private TextView aadharResult, panResult, countText;

    private Uri sourceUri,destinationUri ;
    private String finalAadhar, finalPan;

    private final List<String> allDocs = new ArrayList<>();

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //        region Initialization of views
        aadharButton = findViewById(R.id.aadharButton);
        panButton = findViewById(R.id.panButton);
        Button exportButton = findViewById(R.id.exportButton);
        Button clearButton = findViewById(R.id.clearButton);
        Button addButton = findViewById(R.id.addButton);

        aadharCheck = findViewById(R.id.aadharCheck);
        panCheck = findViewById(R.id.panCheck);

        aadharResult = findViewById(R.id.aadharResult);
        panResult = findViewById(R.id.panResult);
        countText = findViewById(R.id.countText);

        sourceUri = Uri.fromFile(new File(getExternalCacheDir().getAbsolutePath() + "/rawPic.jpg"));
        destinationUri = Uri.fromFile(new File(getExternalCacheDir().getAbsolutePath() + "/finalPic.jpg"));

        //endregion

        requestPermissions(new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 10);

        addButton.setOnClickListener(view -> {
            if (aadharCheck.isChecked() && panCheck.isChecked()) {
                allDocs.add(finalAadhar);
                allDocs.add(finalPan);
                changeCount(false);

                aadharButton.setVisibility(View.VISIBLE);
                aadharResult.setVisibility(View.GONE);
                aadharCheck.setChecked(false);
                finalAadhar = null;

                panButton.setVisibility(View.VISIBLE);
                panResult.setVisibility(View.GONE);
                panCheck.setChecked(false);
                finalPan = null;
            } else toast("Please scan all documents!");
        });

        clearButton.setOnClickListener(view -> {
            allDocs.clear();
            changeCount(true);
        });

        exportButton.setOnClickListener(view -> exportXls());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void readText(Uri resultUri) {
        try {
            InputImage image = InputImage.fromFilePath(this, resultUri);

            TextRecognizer recognizer = TextRecognition.getClient();

            recognizer.process(image)
                    .addOnSuccessListener(visionText -> processText(visionText.getText()))
                    .addOnFailureListener(e -> toast("Couldn't detect the document, try again"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void processText(String text) {
        String pattern = "\\d{4}\\s\\d{4}\\s\\d{4}";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(text);

        if (m.find()) {
            String aadharWithSpaces = Objects.requireNonNull(m.group(0)).trim();
            aadharButton.setVisibility(View.GONE);
            aadharResult.setVisibility(View.VISIBLE);
            aadharResult.setText(aadharWithSpaces);
            aadharCheck.setChecked(true);
            finalAadhar = aadharWithSpaces.replaceAll(" ", "");
        } else {
            List<String> rawPan = Arrays.asList(text.split("\n"));
            int panTextIndex = -1;
            for (String s : rawPan) {
                if (s.toLowerCase().trim().contains("permanent account number")) {
                    panTextIndex = rawPan.indexOf(s);
                    break;
                }
            }
            if (panTextIndex != -1) {
                finalPan = rawPan.get(++panTextIndex);
                panButton.setVisibility(View.GONE);
                panResult.setVisibility(View.VISIBLE);
                panResult.setText(finalPan);
                panCheck.setChecked(true);
            } else toast("Couldn't detect the document, try again");

        }
    }

    private void changeCount(boolean clear) {
        String text;
        if (clear) text = "No. of entries = 0";
        else text = "No. of entries = " + (allDocs.size() / 2);
        countText.setText(text);
    }

    private void getPhoto() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, sourceUri);
        startActivityForResult(takePictureIntent, 1);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                UCrop.of(sourceUri, destinationUri)
                        .withAspectRatio(11, 7)
                        .start(MainActivity.this);
            } else toast("Couldn't get the photo. Try again!");
        }

        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            final Uri resultUri = UCrop.getOutput(Objects.requireNonNull(data));
            readText(resultUri);
        } else if (resultCode == UCrop.RESULT_ERROR) toast("Couldn't get the photo. Try again!");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10) {
            if (grantResults[0] == grantResults[1] && grantResults[1] == -1) {
                toast("Storage permission is required for saving excel file");
                requestPermissions(new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, 10);
            } else {
                aadharButton.setOnClickListener(view -> getPhoto());
                panButton.setOnClickListener(view -> getPhoto());
            }
        }
    }

    void exportXls() {
        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Documents");
            int rowNum = 0;
            Row row = sheet.createRow(rowNum++);
            Cell cell = row.createCell(0);
            Cell cell1 = row.createCell(1);
            Cell cell2 = row.createCell(2);
            cell.setCellValue("Serial No.");
            cell1.setCellValue("Aadhar Number");
            cell2.setCellValue("Pan Number");
            for (int i = 0; i < allDocs.size(); i = i + 2) {
                Row row1 = sheet.createRow(rowNum);
                Cell cell3 = row1.createCell(0);
                Cell cell4 = row1.createCell(1);
                Cell cell5 = row1.createCell(2);
                cell3.setCellValue(rowNum++);
                cell4.setCellValue(allDocs.get(i));
                cell5.setCellValue(allDocs.get(i + 1));
            }
            FileOutputStream out = new FileOutputStream(new File(Environment.getExternalStorageDirectory() + "/Final.xlsx"));
            workbook.write(out);
            out.close();
            toast("Excel written successfully on disk.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void toast(Object o) {
        Toast.makeText(this, o.toString(), Toast.LENGTH_LONG).show();
    }
}
